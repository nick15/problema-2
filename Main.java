import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main {
	private int n;
	private int m;
	private int [][]matrix;
	private  List<Element> elements;
	private  int maximum;
	
	public static void main(String[] args) throws IOException
	{
		Main m = new Main();
		m.elements = new ArrayList<Element>();
		m.maximum = 0;
		try
		{
			m.ReadFile();
			for(int i = 0; i < m.n; i++)
				for(int j = 0; j < m.n; j++)
				{
					m.MaxInRow(i,j);
					m.MaxInColumn(i,j);
					m.MaxInDiagonalPrimary(i,j);
					m.MaxInDiagonalSecondary(i,j);
				}
		
		
		m.Display();
		}catch(Exception ex){
			System.out.println(ex);
		}
		
		
	}
	
	public void ReadFile() throws IOException
	{
		Scanner s = new Scanner(new File("in.txt"));
		this.n = s.nextInt();
		this.m = s.nextInt();
		
		if(this.m > this.n)
			{
			s.close();
			throw new IOException("Number of M elements is greater than N!");	
			}
		
		this.matrix = new int[this.n][this.n];
		for(int i = 0; i < this.n; i++)
			for(int j = 0; j < this.n; j++)
				this.matrix[i][j] = s.nextInt();
		s.close();	
		
	}
	
	public void Display() throws IOException
	{	
		Element el;
		int k;
		System.out.println(this.n + " ");
		System.out.println(this.m + " ");
		for(int i = 0; i < this.n; i++)
		{
			for(int j = 0; j < this.n; j++)
				System.out.print(this.matrix[i][j] + " ");
			System.out.println("");
		}
		
		System.out.println("Results:");
		for(int i = 0; i < this.elements.size(); i++)
		{
		   el=this.elements.get(i);
		   switch(el.getType())
		   {
		   case 1:	//row
			   for( k = el.getColumn(); k < el.getColumn() + this.m - 1; k++)
					  System.out.print(this.matrix[el.getRow()][k] + " * ");
				  System.out.println(this.matrix[el.getRow()][k] + " = " + this.maximum);
				  break;
		   case 2:	//column
			   for( k = el.getRow(); k < el.getRow() + this.m - 1; k++)
					  System.out.print(this.matrix[k][el.getColumn()] + " * ");
				  System.out.println(this.matrix[k][el.getColumn()] + " = " + this.maximum);
				  break;
		   case 3:	//DiagonalPrimary
			   for( k = 0; k < this.m - 1; k++)
					  System.out.print(this.matrix[k+el.getRow()][k+el.getColumn()] + " * ");
				  System.out.println(this.matrix[k+el.getRow()][k+el.getColumn()] + " = " + this.maximum); 
				  break;
		   case 4:	//DiagonalSecondary
			   for( k = 0; k < this.m - 1; k++)
					  System.out.print(this.matrix[k+el.getRow()][el.getColumn()-k] + " * ");
				  System.out.println(this.matrix[k+el.getRow()][el.getColumn()-k] + " = " + this.maximum); 
				  break;
		   }
		 
		} 
	}
	
	public void MaxInRow(int i,int j)
	{
		int isValid;
		int product = 1;
		if( j + this.m - 1 < this.n)
			isValid = 1;
		else isValid = 0;
		
		if(isValid == 1)
		{
			for(int k = j; k < j + this.m; k++)
				product *= this.matrix[i][k];

			if(product > this.maximum)
			{
				this.maximum = product;
				Element el = new Element(i,j,1);
				this.elements.clear();
				this.elements.add(el);
			}
			else if(product == this.maximum)
			{
				Element el = new Element(i,j,1);//type 1
				this.elements.add(el);
			}
		}

	}
	
	public void MaxInColumn(int i, int j)
	{
		int isValid;
		int product = 1;
		if( i + this.m - 1 < this.n)
			isValid = 1;
		else isValid = 0;
		
		if(isValid == 1)
		{
			for(int k = i; k < i + this.m; k++)
				product *= this.matrix[k][j];
				
			if(product > this.maximum)
			{
				this.maximum = product;
				Element el = new Element(i,j,2);
				this.elements.clear();
				this.elements.add(el);
			}
			else if(product == this.maximum)
			{
				Element el = new Element(i,j,2);//type 2
				this.elements.add(el);
			}
		}
	}
	
	public void MaxInDiagonalPrimary(int i, int j)
	{
		int isValid;
		int product = 1;
		if( i + this.m - 1 < this.n && j + this.m - 1 < this.n)
			isValid = 1;
		else isValid = 0;
		
		if(isValid == 1)
		{
			for(int k = 0; k < this.m; k++)
				product *= this.matrix[i+k][j+k];
				
			if(product > this.maximum)
			{
				this.maximum = product;
				Element el = new Element(i,j,3);
				this.elements.clear();
				this.elements.add(el);
			}
			else if(product == this.maximum)
			{
				Element el = new Element(i,j,3);//type 3
				this.elements.add(el);
			}
		}
	}
	
	public void MaxInDiagonalSecondary(int i, int j)
	{
		int isValid;
		int product = 1;
		if( i + this.m - 1 < this.n && j - this.m + 1 >= 0)
			isValid = 1;
		else isValid = 0;
		
		if(isValid == 1)
		{
			for(int k = 0; k < this.m; k++)
				product *= this.matrix[i+k][j-k];
				
			if(product > this.maximum)
			{
				this.maximum = product;
				Element el = new Element(i,j,4);
				this.elements.clear();
				this.elements.add(el);
			}
			else if(product == this.maximum)
			{
				Element el = new Element(i,j,4);//type 4
				this.elements.add(el);
			}
		}
	}
}
