
public class Element {

	private int row;
	private int column;
	private int type; //1-row 2-column 3-diagonal primary 4-diagonal secondary
	
	public Element(int row, int column, int type) {
		super();
		this.row = row;
		this.column = column;
		this.type = type;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
